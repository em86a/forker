<?php

namespace Otzy\Forker;

class ForkerFactory
{

    /**
     * @var ForkerFactory
     */
    private static $instance;

    /**
     * @return ForkerInterface
     * @throws 
     */
    public static function getInstance()
    {
        if (!(self::$instance instanceof ForkerInterface)) {
            if (is_callable('pcntl_fork')){
                self::$instance = new PcntlForker();
            }else{
                throw new \Exception('Process control extension is not installed');
            }
        }

        return self::$instance;
    }

    private function __construct()
    {
        
    }
}